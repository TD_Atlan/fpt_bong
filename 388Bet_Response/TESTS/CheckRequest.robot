*** Settings ***
Resource          ../GLOBAL/Pages.robot

*** Test Cases ***
TC00_Register
    Register Random Account

TC01_HomePage
    [Setup]    Go To Home Page
    Check HomePage
    [Teardown]

TC03_Sports
    [Setup]    Go To Home Page
    Check Sports
    [Teardown]

TC04_Casino
    [Setup]    Go To Home Page
    [Template]
    Check Casino
    [Teardown]

TC05_Games
    [Setup]    Go To Home Page
    [Template]
    Check Games
    [Teardown]

TC06_Helps
    [Setup]    Go To Home Page
    [Template]
    Check Helps
    [Teardown]
